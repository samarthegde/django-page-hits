from django.db import models


class PageHit(models.Model):
    """
    Model to save the hit counts for any urls.
    """
    url = models.CharField(('URL'), max_length=255)
    visit_date = models.DateField(('Visit date'), editable=False)
    modified_at = models.DateTimeField(('Modified'), auto_now=True,
                                       editable=False)
    anonymous_hits = models.PositiveIntegerField(('Anonymous user Hits'),
                                                 default=0)
    created_at = models.DateTimeField(('Created'), auto_now_add=True,
                                      editable=False)
    registered_hits = models.PositiveIntegerField(('Registered user hits'),
                                                  default=0)

    class Meta:
        ordering = ('-visit_date', '-modified_at')