2. Add *'django_page_hits'* at the end of **INSTALLED_APPS**.

3. Add *'django_page_hits.middleware.PageHitsMiddleware'* to end of **MIDDLEWARE_CLASSES**.

4. Run `python manage.py syncdb` or `python manage.py migrate`.

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
